<html>
<head>
	<meta charset="utf-8">
	<link rel="icon" type="image/gif/png" href="logo.png">
	<title>Koduleht</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.toggle').click(function(){
			jQuery('#main-form').slideToggle();
		});
	});
	</script>
	<?php
	session_start();

	unset($_SESSION['usererror']);

	if (!isset($_SESSION['username']))
	{
		header('Location: login.php');
	} else {
		unset($_SESSION['error']);
	}

	// Database connection
	$host = "localhost";
	$user = "test";
	$pass = "t3st3r123";
	$db = "test";

	$conn= mysqli_connect($host, $user, $pass, $db);
	mysqli_query($conn, "SET CHARACTER SET UTF8") or
	die("Error, ei saa andmebaasi charsetti seatud");

	$sql = "SELECT * FROM berit_results";

	$result = mysqli_query($conn, $sql);

	?>
</head>
<body>

	<div class="center">

		<h1>Tulemused</h1>

		<div class="toggle">Peida/näita vorm</div>

		<a class="about-btn" href="about.php">MINUST</a> <a class="add-btn adding" href="add.php">LISA</a>

		<?php if (isset($_SESSION['username'])): ?>
			<a href="logout.php" class="logout">Logi välja</a>
		<?php endif; ?>

		<form id="main-form" action="send.php" method="post">

			<p>Kuupäev:</p>
			<input id="date_value" type="text" value="<?php echo date('d.m.Y'); ?>" name="date_value">

			<p>Ala:</p>
			<input id="field" type="text" name="field">

			<p>Võistlus nimi:</p>
			<input id="name_comp" type="text" name="name_comp">

			<p>Võistlus url:</p>
			<input id="comp_url" type="text" value="#" name="comp_url">

			<p>Klass:</p>
			<input id="c" type="text" name="class">

			<p>Klass url:</p>
			<input id="class_url" type="text" value="#" name="class_url">

			<p>Raskusaste:</p>
			<input id="level" type="text" name="level">

			<p>Hobune:</p>
			<input id="horse" type="text" name="horse">

			<p>Hobune url:</p>
			<input id="horse_url" type="text" value="#" name="horse_url">

			<p>Koht:</p>
			<input id="place" type="text" name="place">

			<p>Tulemus:</p>
			<input id="result" type="text" name="result">

			<input id="submit" type="submit">

		</form>

		<div id="results-wrapper">

			<table cellspacing="0" id="results">

				<tr>
					<th>Kuupäev</th>
					<th>Ala</th>
					<th>Nimi</th>
					<th>Klass</th>
					<th>Raskus</th>
					<th>Horsi</th>
					<th>Koht</th>
					<th>tulemus</th>
					<th>kustuta</th>
				</tr>

				<?php while ($rows = mysqli_fetch_array($result, MYSQLI_NUM)): ?>

					<tr>

						<td><?php echo $rows[1]; ?></td>
						<td><?php echo $rows[2]; ?></td>
						<td><?php echo '<a href="http://' . $rows[4] . '">' . $rows[3] . '</a>'; ?></td>
						<td><?php echo '<a href="http://' . $rows[6] . '">' . $rows[5] . '</a>'; ?></td>
						<td><?php echo $rows[7]; ?></td>
						<td><?php echo '<a href="http://' . $rows[9] . '">' . $rows[8] . '</a>'; ?></td>
						<td><?php echo $rows[10]; ?></td>
						<td><?php echo $rows[11]; ?></td>
						<td><a href="delete.php?id=<?php echo $rows[0]; ?>">X</a></td>

					</tr>

				<?php endwhile; ?>

			</table>
		</div>
	</div>

</body>
</html>
