<html>
<head>
	<meta charset="utf-8">
	<link rel="icon" type="image/gif/png" href="logo.png">
	<title>Minust</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>

	<?php
	session_start();
	unset($_SESSION['usererror']);
	if (!isset($_SESSION['username'])) {
		header('Location: login.php');
	} else {
		unset($_SESSION['error']);
	}
	?>
</head>

<body>

	<div class="center">
		<h1>Minust</h1>
		<a class="about-btn" href="index.php">AVALEHT</a>
		<?php if (isset($_SESSION['username'])): ?>
			<a href="logout.php" class="logout">Logi välja</a>
		<?php endif; ?>

		<div class="about">

			<p>
				Minu nimi on Berit Põldoja ja ma olen ratsasportlane, täpsemalt öeldes noor takistussõitja.
				Olen väga aktiivne ja positiivne inimene. Selle tänu võlgenen just oma hobusele Bücarole. Temaga koos arenededa on võrratu. Sport sai osaks minu elust juba lapsepõlvest, kui kasvasin üles, jälgides, kuidas minu ema, endise ratsasportlasena ratsutab. Tema on aidanud mind ka kõige keerulistemal aegadel nii treeneri kui ka ema rollis. Õpin Eesti Infotehnoloogia Kolledžis arenduse erialal. Minu jaoks on IT ja ratsasport väga sarnased, mõlemates saab pidevalt edasi areneda ning raskuste ületamine mistahes olukorrast on magus võit.
				Võistlemisega alustades püstitasin endale alati väiksemaid eesmärke, samal ajal unistades suurtest. Minu inspiratsiooniks on olnud alati Bücaro, kelle silmis on näha kõiki neid emotsioone, mida mina tunnen tema seljas olles. Bücarol on tahe ratsanikku aidata igas situatsioonis. Anname alati endast parima ning loobumis võimalust ei ole kunagi olnud. Meie teekond on olnud väga kirev. Oleme õppinud teineteist tundma ja seal hulgas vaieldamatult kokku kasvanud.  Tänu sellele on ka tulemused iga korraga paremaks läinud.
			</p>

		</div>
	</div>

</body>
</html>
