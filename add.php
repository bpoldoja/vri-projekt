<html>
<head>
	<meta charset="utf-8">
	<link rel="icon" type="image/gif/png" href="logo.png">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
	<title>Lisa</title>

	<?php
	session_start();
	unset($_SESSION['usererror']);
	if (!isset($_SESSION['username'])) {
		header('Location: login.php');
	} else {
		unset($_SESSION['error']);
	}
	?>
</head>

<body onload="loadTodos()">

	<div class="center">
		<h1>Lisa</h1>
		<a class="index-btn" href="index.php">AVALEHT</a>

		<?php if (isset($_SESSION['username'])): ?>
			<a href="logout.php" class="logout">Logi välja</a>
		<?php endif; ?>

		<div class="add">
			<p class="text">

				<label id="items " for="todo-input">LISAN HARJUTUSI / TO-DO</label>
				<input  id="todo-input" type="text" size="30">

				<button id="btnAdd" onclick="onAddClicked()">+</button>
			</p>

			<ul id="todo-list">

			</ul>
			<script type="text/javascript" src="add.js"></script>
		</div>
	</div>

</body>
</html>
