<?php session_start(); unset($_SESSION['usererror']); ?>
<head>
	<meta charset="utf-8">
	<link rel="icon" type="image/gif/png" href="logo.png">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div class="center">
		<table class="form-table" width="300" border="0" align="center" cellpadding="0" cellspacing="1">
			<tr>
				<form name="form1" method="post" action="checklogin.php">
					<td>
						<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
							<tr>
								<td colspan="3"><strong>Member Login </strong></td>
							</tr>
							<tr>
								<td width="78">Username</td>
								<td width="6"></td>
								<td width="294"><input name="myusername" type="text" id="myusername"></td>
							</tr>
							<tr>
								<td>Password</td>
								<td></td>
								<td><input name="mypassword" type="password" id="mypassword"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><input type="submit" name="Submit" value="Login"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><a href="register.php">Registreeri</a></td>
							</tr>
						</table>
					</td>
				</form>
			</tr>
		</table>
		<?php if (isset($_SESSION['error'])): ?>
			<h2 style="float:left; width:100%; text-align:center; margin-top:20px;">Vale parool</h2>
		<?php endif; ?>
		<?php if (isset($_SESSION['regdone'])): ?>
			<h2 style="float:left; width:100%; text-align:center; margin-top:20px;">Kasutaja loodud</h2>
		<?php endif; ?>
	</div>
</body>
