//Takes to-do label as parameter.
//Creates a new li with a span and button.
function createNewTodo(todoText, save) {

    if (save == true) {
        var list = (localStorage["todo-list"] != null) ? JSON.parse(localStorage["todo-list"]) : [];
        list.push(todoText);
        localStorage["todo-list"] = JSON.stringify(list);
    }

    //Make div
    var li = document.createElement("li");
    
    //Make paragraph
    var span = document.createElement("span");
    span.innerHTML = todoText;

    //Make remove button
    var button = document.createElement("button");
    button.onclick = function() {
        removeToDo(li);
    };
    button.innerHTML = "-";

    //Put span and button in li
    li.appendChild(span);
    li.appendChild(button);

    return li
}

function addTodoToList(todo) {
    var todo_list = document.getElementById("todo-list");
    todo_list.appendChild(todo);
}

//Makes a new lbl and returns the input empty
function getToDoLbl() {
    var todo_input = document.getElementById("todo-input");
    var val = todo_input.value;
    todo_input.value = "";
    return val;
}

// Removes an element
function removeToDo(element) {
    if (localStorage["todo-list"] != null) {
        var list = JSON.parse(localStorage["todo-list"])
        var text = element.getElementsByTagName("span")[0].innerHTML
        list.splice(list.indexOf(text), 1)
        localStorage["todo-list"] = JSON.stringify(list)
    }
    loadTodos()
}

//When clicked + button creates new to-do li
function onAddClicked() {
    var newToDoLbl = getToDoLbl();
    var todo = createNewTodo(newToDoLbl, true);
    addTodoToList(todo);
}

function loadTodos() {

    var todo_list = document.getElementById("todo-list");
    while (todo_list.firstChild) {
        todo_list.removeChild(todo_list.firstChild)
    }
    if (localStorage["todo-list"] != null) {
        var loadedList = JSON.parse(localStorage["todo-list"] || []);
        for (var i = 0; i < loadedList.length; i++) {
            var todo = createNewTodo(loadedList[i], false);
            addTodoToList(todo);
        }
    }
}
